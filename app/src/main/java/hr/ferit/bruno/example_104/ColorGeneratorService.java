package hr.ferit.bruno.example_104;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Random;

public class ColorGeneratorService extends Service {
    private final ColorBinder mColorBinder = new ColorBinder();
    private final Random mRandomGenerator = new Random();

    @Override
    public IBinder onBind(Intent intent) { return this.mColorBinder; }

    public int generateRandomColor(){
        int r = this.mRandomGenerator.nextInt(256);
        int g = this.mRandomGenerator.nextInt(256);
        int b = this.mRandomGenerator.nextInt(256);
        return Color.rgb (r, g, b);
    }

    public class ColorBinder extends Binder {
        ColorGeneratorService getService(){
            return ColorGeneratorService.this;
        }
    }
}
