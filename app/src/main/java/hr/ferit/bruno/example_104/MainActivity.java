package hr.ferit.bruno.example_104;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "Service";

    @BindView(R.id.tvHelloWorld) TextView tvHelloWorld;
    @BindView(R.id.rlRootView) RelativeLayout rlRootView;

    ColorGeneratorService mColorService;
    ColorServiceConnection mColorServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.mColorServiceConnection = new ColorServiceConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(getApplicationContext(), ColorGeneratorService.class);
        this.bindService(intent, this.mColorServiceConnection, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "Bind service");
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unbindService(this.mColorServiceConnection);
        Log.d(TAG, "Unbind service");
    }

    @OnClick(R.id.rlRootView)
    public void onClick(View v) {
        if(this.mColorService != null){
            rlRootView.setBackgroundColor(this.mColorService.generateRandomColor());
            tvHelloWorld.setTextColor(this.mColorService.generateRandomColor());
        }
    }

    class ColorServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mColorService = ((ColorGeneratorService.ColorBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mColorService = null;
        }
    }
}

